package main

import (
	"flag"
	"fmt"
	"os"
	"udemy/dictionary/dictionary"
)

func main() {
	action := flag.String("action", "list", "Action to perform on the dictionary")

	d, err := dictionary.New("./badger")
	handleErr(err)
	defer d.Close()

	flag.Parse()

	switch *action {
	case "list":
		actionList(d)
	case "add":
		actionAdd(d, flag.Args())
	case "define":
		actionDefine(d, flag.Args())
	case "delete":
		actionDelete(d, flag.Args())
	default:
		fmt.Printf("Unknown action: %v\n", *action)
	}
}

func actionList(d *dictionary.Dictionary) {
	handleErr(d.PrintAll())
}

func actionAdd(d *dictionary.Dictionary, args []string) {
	word := args[0]
	def := args[1]
	handleErr(d.Add(word, def))
	fmt.Printf("Word %s added to the dictionary\n", word)
}

func actionDefine(d *dictionary.Dictionary, args []string) {
	word := args[0]
	entry, err := d.Get(word)
	handleErr(err)
	fmt.Println(entry)
}

func actionDelete(d *dictionary.Dictionary, args []string) {
	word := args[0]
	handleErr(d.Remove(word))
}

func handleErr(err error) {
	if err != nil {
		fmt.Printf("Dictionary error: %v\n", err)
		os.Exit(1)
	}
}
